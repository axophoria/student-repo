# Exercise 5: Ansible Variables and Facts



## Table of Contents

- [Exercise 5: Ansible Variables and Facts](#exercise-5-ansible-variables-and-facts)
  - [Table of Contents](#table-of-contents)
  - [Objective](#objective)
  - [Guide](#guide)
    - [Step 1 - Ansible Variables](#step-1---ansible-variables)
      - [Variables in Inventory](#variables-in-inventory)
      - [Variables in host\_vars and group\_vars](#variables-in-host_vars-and-group_vars)
      - [Variables in vars files](#variables-in-vars-files)
      - [Variables directly in playbook](#variables-directly-in-playbook)
    - [Step 2 - Using `facts` modules](#step-2---using-facts-modules)
    - [Step 3 - Create the facts task](#step-3---create-the-facts-task)
    - [Step 4 - Executing the playbook](#step-4---executing-the-playbook)
    - [Step 5 - Using debug module](#step-5---using-debug-module)
  - [Exercise Challenges](#exercise-challenges)
    - [Challenge 1](#challenge-1)
    - [Challenge 2](#challenge-2)
    - [Challenge 3](#challenge-3)
  - [Takeaways](#takeaways)
  - [Solution](#solution)
  - [Complete](#complete)

## Objective

The objective for this exercise it to understand the use of variables and facts in Ansible.

Ansible uses variables to manage differences between systems. With Ansible, you can execute tasks and playbooks on multiple different systems with a single command. To represent the variations among those different systems, you can create variables with standard YAML syntax, including lists and dictionaries. You can define these variables in your playbooks, in your inventory, in re-usable files or roles, or at the command line. You can also create variables during a playbook run by registering the return value or values of a task as a new variable.

After you create variables, you can use them in module arguments, in conditional “when” statements, in templates, and in loops. The ansible-examples github repository contains many examples of using variables in Ansible.

Ansible facts are information derived from speaking to the remote network elements.  Ansible facts are returned in structured data (JSON) that makes it easy manipulate or modify.  For example a network engineer could create an audit report very quickly using Ansible facts and templating them into a markdown or HTML file.

This exercise will cover:

* host and group vars
* Using network `facts` modules to gather facts
* Using Variables in playbooks


## Guide

### Step 1 - Ansible Variables

#### Variables in Inventory

Either use Visual Studio Code or use the `cat` command to view the contents of the `~/lab_inventory/hosts` file.

```bash
$ cat ~/lab_inventory/hosts
```

```ini
[routers:vars]
ansible_user=ec2-user

[routers:children]
cisco
juniper
arista

[cisco]
rtr1 ansible_host=18.221.98.174 private_ip=172.16.104.14
[arista]
rtr2 ansible_host=18.224.61.142 private_ip=172.18.4.162
rtr4 ansible_host=3.133.116.144 private_ip=172.18.244.56
[juniper]
rtr3 ansible_host=18.119.161.229 private_ip=172.16.64.240

[cisco:vars]
ansible_network_os=ios
ansible_connection=network_cli

[juniper:vars]
ansible_network_os=junos
ansible_connection=netconf

[arista:vars]
ansible_network_os=eos
ansible_connection=network_cli
ansible_become=true
ansible_become_method=enable

[dc1]
rtr1
rtr3

[dc2]
rtr2
rtr4

[control]
ansible-1 ansible_host=18.191.202.234 ansible_user=ec2-user private_ip=172.16.215.122

[network:children]
routers

[network:vars]
restore_inventory="Workshop Inventory"
restore_credential="Workshop Credential"
restore_project="Workshop Project"
```

You will see a number of variables defined in the workshop inventory file.  As the example shows, variables can be defined for a specific host, or for a group of hosts.  

Variables like `ansible_host` would be defined at a host level because they specify the IP to connect to a host on (which may be different then the IP returned for the FQDN).

There are serveral variables defined in the inventory file that would typically by defined at a group level. In this inventory there are several variables that are important for using Ansible in network automation; specifically the `ansible_connection` and `ansible_network_os` variables.  The `ansible_connection` variable is used to determine which connection plugin to use to connect to the endpoint.  The `ansible_network_os` variable is a requirement when you use the network_cli connection type.

```ini
[cisco:vars]
ansible_network_os=ios
ansible_connection=network_cli

[juniper:vars]
ansible_network_os=junos
ansible_connection=netconf

[arista:vars]
ansible_network_os=eos
ansible_connection=network_cli
```


#### Variables in host_vars and group_vars

Variables can also be defined in directories in a project.  You can use the host_vars directory to define host specific variables, and group_vars to define variables for different groups in your inventory.  These variable files are automatically loaded and the variables associated with endpoints based on their hostname and the groups they are in.

**Host Vars example:**

./`{project directory}`/`host_vars`/`{host name}`

**Group Vars examples:**

./`{project directory}`/`group_vars`/`{group name as directory}`/`{individual variable files}`  
and  
./`{project directory}`/`group_vars`/`{group name as file}`    
and  
./`{project directory}`/`group_vars`/`all`

Create a group vars file for all cisco devices; you can do this in Visual Studio Code or in the terminal window:
```bash
[student@ansible-1 ~]$ cd ~/student-repo
[student@ansible-1 student-repo]$ mkdir group_vars
[student@ansible-1 student-repo]$ echo "ansible_network_os: ios" > group_vars/cisco
[student@ansible-1 student-repo]$ cat group_vars/cisco
ansible_network_os: ios
```

You can now open the ~lab_inventory/hosts file and comment out the line with ansible_network_os in the [cisco:vars] section.

```ini
[cisco:vars]
#ansible_network_os=ios
ansible_connection=network_cli
```

We will again use the ping module to connect to rtr1 to verify we can still connect to the endpoints in the cisco group.

```bash
[student@ansible student-repo]$ ansible -m ping rtr1
[WARNING]: ansible-pylibssh not installed, falling back to paramiko
rtr1 | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
```

Go back to the ~/lab_inventory/hosts and uncomment the ansible_network_os=ios under the Cisco group.
```ini
[cisco:vars]
ansible_network_os=ios
ansible_connection=network_cli
```


If successful, you should see `SUCCESS`, as well as a `pong` response next to the `ping`.

#### Variables in vars files

You can also create a variables file and then include that variables file directly in your playbook.  To include a variable file in the playbook, use the `vars_files` key in your playbook.

Create the var file (vars.yml):
```yaml
---
var_from_vars_file: "hello world"

...
```


```yaml
---
- name: an example playbook
  hosts: rtr1
  vars_files:
    - ./vars.yml
  tasks:
  - name: Print out vars
    ansible.builtin.debug:
      msg: "{{ var_from_vars_file }}"

...
```

Use Visual Studio Code or vi to create a playbook called vars_playbook.yml in the ~/student_repo/project_files/ to see this in action.

In the terminal window, run the playbook.

```bash

[student@ansible-1 ~] cd ~/student_repo/project_files
[student@ansible-1 project_files]$ ansible-navigator run -m stdout ~/vars_playbook.yml
```

#### Variables directly in playbook

You can also create a variable directly in a playbook at the play level, or at the task level.  The below playbook sets a variable at the play level, then in the tasks prints that variable out.  In the second task, it overwrites the variable at the task level, and then prints it out.


```yaml
---
- name: an example playbook
  hosts: all
  vars:
    a_variable: "Hello World"
  
  tasks:
  - name: Print out vars
    ansible.builtin.debug:
      msg: "{{ a_variable }}"

  - name: Print out task vars
    vars:
      a_variable: "Hello World!"
    ansible.builtin.debug:
      msg: "{{ a_variable }}"

...
```

If you put this content in a playbook file and run it, you will see how you can set variables at different levels to achieve different results.


For more information on variables and on variable precedence, you can review the documentation at:
https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_variables.html


### Step 2 - Using `facts` modules 

Create a new file in Visual Studio code:
![vscode new file](images/vscode_new_file.png)

For simplicity please name the playbook: `facts.yml`:
![vscode save file](images/vscode_save_as.png)


Enter the following play definition into `facts.yml`:

```yaml
---
- name: gather information from routers
  hosts: cisco
  gather_facts: no
```

### Step 3 - Create the facts task

Next, add the first `task`. This task will use the `cisco.ios.facts` module we looked at in the last exercise to gather facts about each device in the group `cisco`.

```yaml
---
- name: gather information from routers
  hosts: cisco
  gather_facts: no

  tasks:
    - name: gather router facts
      cisco.ios.facts:
```

> Note:
>
> A play is a list of tasks. Modules are pre-written code that perform the task.

Save the playbook.

### Step 4 - Executing the playbook

Execute the Ansible Playbook by running `ansible-navigator`:

```sh
$ ansible-navigator run facts.yml
```

This will open an interactive session while the playbook interacts:

Screenshot of facts.yml:
![ansible-navigator run facts.yml](images/ansible-navigator-facts.png)

To zoom into the playbook output we can press **0** which will show us a host-centric view.  Since there is only one host, there is just one option.

Screenshot of zooming in:
![ansible-navigator zoom hosts](images/ansible-navigator-hosts.png)

To see the verbose output of **rtr1** press **0** one more time to zoom into the module return values.

Screenshot of zooming into module data:
![ansible-navigator zoom module](images/ansible-navigator-module.png)

You can scroll down to view any facts that were collected from the Cisco network device.

### Step 5 - Using debug module

Write two additional tasks that display the routers' OS version and serial number.

<!-- {% raw %} -->

``` yaml
---
- name: gather information from routers
  hosts: cisco
  gather_facts: no

  tasks:
    - name: gather router facts
      cisco.ios.facts:

    - name: display version
      debug:
        msg: "The IOS version is: {{ ansible_net_version }}"

    - name: display serial number
      debug:
        msg: "The serial number is:{{ ansible_net_serialnum }}"
```

<!-- {% endraw %} -->

Using less than 20 lines of "code" you have just automated version and serial number collection. Imagine if you were running this against your production network! You have actionable data in hand that does not go out of date.


## Exercise Challenges

### Challenge 1

We will now create variables to be used in a playbook based on the earlier sections in this exercise.  

- Create a group variable for the arista group
  - group_var: arista
- Create a host variable for hosts rtr2 and rtr4
  - host_var: {{ ansible_inventory }}
 
Run the playbook and validate that it prints the correct variables.

Try on your own first, but if you need help, you can review the example: [challenge 1](./challenge1_example.md).


### Challenge 2

You may have noticed there isn't a lot of data provided by the get_facts module, and perhaps thought, "there is a lot more information in my network device than that - I wonder how I get the rest of it?"

In the first exercise we looked at collections, and how to use the `ansible-navigator` command to get more information about the modules and plugins available in them.  We just used the cisco.ios.ios_facts module without any arguments to get facts about an endpoint; can you use the `ansible-navigator doc` command to get more information about the module, and to learn how to get more information about your network device?

The challenge is to use the `ios_facts` module to print out information about the interfaces, l2_interfaces, l3_interfaces, vlans, and ntp.  Review the information in the ansible-navigator TUI.

### Challenge 3

Read the variables_vault.md file in the supplemental directory and follow the challenge in the readme.
[Ansible Vault Challenge](../supplemental/variables_vault.md)


## Takeaways

* The [cisco.ios.facts module](https://docs.ansible.com/ansible/latest/collections/cisco/ios/ios_config_module.html) gathers structured data specific for Cisco IOS.  There are relevant modules for each network platform.  For example there is a junos_facts for Juniper Junos, and a eos_facts for Arista EOS.
* The [debug module](https://docs.ansible.com/ansible/latest/modules/debug_module.html) allows an Ansible Playbook to print values to the terminal window.

## Solution

The finished Ansible Playbook is provided here for an answer key: [facts.yml](facts.yml).


## Complete

You have completed lab exercise 5 - Variables and Facts!

---
[Previous Exercise](../4-Playbooks/README.md) | [Next Exercise](../6-Logic/README.md)

[Click here to return to the Ansible Network Automation Workshop](../README.md)
